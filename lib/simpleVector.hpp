#pragma once
template <typename T>
class SimpleVector
{
public:
    SimpleVector()
    :
        mArray(new T[5]), mBegin(&mArray[0]), mEnd(&mArray[0]), mSize(5)
    {
    }
    
    SimpleVector(const int size)
    :
        mArray(new T[size]), mBegin(&mArray[0]), mEnd(&mArray[0]), mSize(size)
    {
    }
    
    SimpleVector(const SimpleVector& rhs)
    :
        mArray(rhs.mArray), mBegin(&mArray[0]), mEnd(&mArray[rhs.size()-1]), mSize(rhs.mSize)
    {
    }
    
    ~SimpleVector()
    {
        delete mArray;
    }
    
    T& operator[] (const int pos); 
    SimpleVector& operator=(const SimpleVector& rhs);
    
    T* begin() { return mBegin; }
    T* end() {return mEnd; }
    int size() const { return (mEnd-mBegin); }
    int capacity() const { return mSize; }
    void emplace_back(const T& rhs);
private:
    T* mArray;
    T* mBegin;
    T* mEnd;
    int mSize;
};

template <typename T>
void SimpleVector<T>::emplace_back(const T& rhs)
{
    if(size() == mSize)
    {
        T* tempArray{new T[mSize*2]};
        tempArray = mArray;
        delete mArray;
        T* mArray{tempArray};
        mBegin = &mArray[0];
        mEnd = &mArray[mSize];
    }
    ++mEnd;
    *mEnd = rhs;
}

template <typename T>
SimpleVector<T>& SimpleVector<T>::operator= (const SimpleVector& rhs)
{
    mArray = rhs.mArray;
    mBegin = &mArray[0];
    mEnd = &mArray[rhs.size()];
    mSize = rhs.mSize;
    return *this;
}

template <typename T>
T& SimpleVector<T>::operator[] (const int pos)
{
    if(pos-1 > mSize)
        throw;
    return mArray[pos];
} 

    
