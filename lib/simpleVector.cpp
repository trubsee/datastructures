#include "simpleVector.hpp"

template <typename T>
void SimpleVector<T>::emplace_back(const T& rhs)
{
    if(size() == mSize)
    {
        T* tempArray{new T[size*2]};
        tempArray = mArray;
        delete mArray;
        T* mArray{tempArray};
        mBegin = &mArray[0];
        mEnd = &mArray[size];
    }
    ++mEnd;
    *mEnd = rhs;
}

template <typename T>
T& SimpleVector<T>::operator[] (const int pos)
{
    if(pos-1 > mSize)
        throw;
    return mArray[pos];
} 

