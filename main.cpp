
#include "lib/simpleVector.hpp"

#define BOOST_TEST_MODULE SimpleVectorTest
#include <boost/test/unit_test.hpp>

#include <iostream>

BOOST_AUTO_TEST_CASE(default_constructor)
{
    SimpleVector<int> testVector;
    BOOST_CHECK_EQUAL(testVector.capacity(), 5);
}

BOOST_AUTO_TEST_CASE(size_constructor)
{
    SimpleVector<int> testVector(10);
    BOOST_CHECK_EQUAL(testVector.capacity(), 10);
}

BOOST_AUTO_TEST_CASE(emplace_back)
{
    SimpleVector<int> testVector(2);
    
    testVector.emplace_back(1);
    testVector.emplace_back(2);
    BOOST_CHECK_EQUAL(testVector.size(),2);
    BOOST_CHECK_EQUAL(testVector.capacity(),2);
    
    testVector.emplace_back(3);
    BOOST_CHECK_EQUAL(testVector.size(),3);
    BOOST_CHECK_EQUAL(testVector.capacity(),4);
}

BOOST_AUTO_TEST_CASE(begin_and_end_pointers)
{
    SimpleVector<int> testVector;
    testVector.emplace_back(1);
    BOOST_CHECK_EQUAL(testVector.begin(), testVector.end());

    int* begin = testVector.begin();
    BOOST_CHECK_EQUAL(*begin, 1);
    *begin = 2;
    BOOST_CHECK_EQUAL(*testVector.begin(), 2);

    testVector.emplace_back(3);
    int* end = testVector.end();
    BOOST_CHECK_EQUAL(*end, 3);
    *end = 4;
    BOOST_CHECK_EQUAL(*end, 4);
}

BOOST_AUTO_TEST_CASE(test_index_operator)
{
    SimpleVector<int> testVector;
    
    testVector.emplace_back(1);
    int value = testVector[0];
    BOOST_CHECK_EQUAL(value, 1);
    value = 2;
    BOOST_CHECK_EQUAL(testVector[0], 2);
}
